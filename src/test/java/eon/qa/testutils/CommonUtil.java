/**
	 * @author Praveen Kalliyath
	 */
package eon.qa.testutils;

import static com.jayway.jsonpath.JsonPath.using;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.regex.Pattern;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;

import javax.xml.parsers.*;
import org.w3c.dom.*;
import javax.xml.xpath.*;

public class CommonUtil {

	private static DateFormat dateFormat;
	private static Date date;
	private static String[] splitedString;

	/**
	 * Method to use default wait
	 * 
	 * @param seconds
	 */
	public static void sleep(int seconds) {
		try {
			System.out.println("Waiting for " + seconds + " seconds");
			Thread.sleep(seconds * 1000);
			System.out.println("Waited for " + seconds + " seconds");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to retrieve random alphabets
	 * 
	 * @param len
	 * 
	 * @return random alphabets
	 */
	public static String getRandomAlpha(int len) {
		char ch[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
		char c[] = new char[len];
		SecureRandom random = new SecureRandom();
		for (int i = 0; i < len; i++) {
			c[i] = ch[random.nextInt(ch.length)];
		}
		System.out.println("Random Alphabet: " + new String(c));
		return new String(c);
	}

	/**
	 * Method to retrieve random charaacters
	 * 
	 * @param len
	 * 
	 * @return random alphabets
	 */
	public static String getRandomChars(int len) {
		char ch[] = "~`!@#$%^&*() {}[]:\'\",./?><;".toCharArray();
		char c[] = new char[len];
		SecureRandom random = new SecureRandom();
		for (int i = 0; i < len; i++) {
			c[i] = ch[random.nextInt(ch.length)];
		}
		System.out.println("Random Alphabet: " + new String(c));
		return new String(c);
	}

	/**
	 * Method to retrieve random mix of alphabets & numbers
	 * 
	 * @param len
	 * 
	 * @return random alphaNumeric
	 */
	public static String getRandomAlphaNumeric(int len) {
		char ch[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
		char c[] = new char[len];
		SecureRandom random = new SecureRandom();
		for (int i = 0; i < len; i++) {
			c[i] = ch[random.nextInt(ch.length)];
		}
		System.out.println("Random AlphaNumeric: " + new String(c));
		return new String(c);
	}

	/**
	 * Method to retrieve random mix of alphabets, numbers & characters
	 * 
	 * @param len
	 * 
	 * @return random alphaNumeric
	 */
	public static String getRandomAlphaNumericChars(int len) {
		char ch[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~`!@#$%^&*() {}[]:\\'\\\",./?><;"
				.toCharArray();
		char c[] = new char[len];
		SecureRandom random = new SecureRandom();
		for (int i = 0; i < len; i++) {
			c[i] = ch[random.nextInt(ch.length)];
		}
		System.out.println("Random AlphaNumeric: " + new String(c));
		return new String(c);
	}

	/**
	 * Method to retrieve random mix of numbers & characters
	 * 
	 * @param len
	 * 
	 * @return random alphaNumeric
	 */
	public static String getRandomNumericChars(int len) {
		char ch[] = "0123456789~`!@#$%^&*() {}[]:\\'\\\",./?><;".toCharArray();
		char c[] = new char[len];
		SecureRandom random = new SecureRandom();
		for (int i = 0; i < len; i++) {
			c[i] = ch[random.nextInt(ch.length)];
		}
		System.out.println("Random AlphaNumeric: " + new String(c));
		return new String(c);
	}

	/**
	 * Method to retrieve random mix of alphabets & characters
	 * 
	 * @param len
	 * 
	 * @return random alphaNumeric
	 */
	public static String getRandomAlphaChars(int len) {
		char ch[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~`!@#$%^&*() {}[]:\\'\\\",./?><;"
				.toCharArray();
		char c[] = new char[len];
		SecureRandom random = new SecureRandom();
		for (int i = 0; i < len; i++) {
			c[i] = ch[random.nextInt(ch.length)];
		}
		System.out.println("Random AlphaNumeric: " + new String(c));
		return new String(c);
	}

	/**
	 * Method to retrieve random number
	 * 
	 * @param len
	 * 
	 * @return random number
	 */
	public static String getRandomNumber(int len) {
		char ch[];
		if (len == 1 || len == 2)
			ch = "123456789".toCharArray();
		else
			ch = "0123456789".toCharArray();

		char c[] = new char[len];
		SecureRandom random = new SecureRandom();
		for (int i = 0; i < len; i++) {
			c[i] = ch[random.nextInt(ch.length)];
		}
		System.out.println("Random Number: " + new String(c));
		return new String(c);
	}

	/**
	 * Method to retrieve random number
	 * 
	 * @param min
	 * 
	 * @param max
	 * 
	 * @return random number within range of minimum value and maximum value
	 */
	public static String getRandomNumberInRange(int min, int max) {
		if (min > max) {
			throw new IllegalArgumentException("Minimum value provided is greater that maximum");
		}
		Random random = new Random();

		return String.valueOf(random.nextInt((max - min) + 1) + min);
	}

	/**
	 * Method to validate regular expression
	 * 
	 * @param value
	 * 
	 * @param expression
	 * 
	 * @return boolean
	 */
	public static boolean regexValidator(String value, String expression) {
		Pattern pattern = Pattern.compile(expression);
		return pattern.matcher(value).matches();
	}

	/**
	 * Method to retrieve todays date
	 * 
	 * @return date
	 */
	public static String returnToday() {
		dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		date = new Date();
		System.out.println("Today's Date: " + dateFormat.format(date));
		return dateFormat.format(date);
	}

	/**
	 * Method to retrieve date in desired pattern
	 * 
	 * @param pattern
	 * 
	 * @return date
	 */
	public static String returnToday(String pattern) {
		dateFormat = new SimpleDateFormat(pattern);
		date = new Date();
		System.out.println("Today's Date: " + dateFormat.format(date));
		return dateFormat.format(date);
	}

	/**
	 * Method to retrieve date in specific pattern
	 * 
	 * @param currentDate
	 * 
	 * @param currentPattern
	 * 
	 * @param requestedPattern
	 * 
	 * @return date
	 */
	public static String convertDateToFormat(String currentDate, String currentPattern, String requestPattern) {
		Date requestedDate = null;
		try {
			requestedDate = new SimpleDateFormat(currentPattern).parse(currentDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println("Requested Date in format: [" + requestPattern + "] :- "
				+ new SimpleDateFormat(requestPattern).format(requestedDate));
		return new SimpleDateFormat(requestPattern).format(requestedDate);
	}

	/**
	 * Convert HashMap to Json
	 * 
	 * @param hashMap
	 * 
	 * @return jsonString
	 */
	public static String convertHashMapToJson(HashMap<String, String> hashMap) {
		ObjectMapper mapper = new ObjectMapper();
		String json = "";
		try {
			System.out.println("Map : " + hashMap);
			json = mapper.writeValueAsString(hashMap);
			System.out.println("Converted Map to Json: " + json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}

	/**
	 * Convert Json to HashMap
	 * 
	 * @param jsonString
	 * 
	 * @return hashMap
	 */
	public static HashMap<String, Object> convertJsonToHashMap(String json) {
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, Object> jsonMap = new HashMap<>();
		try {
			System.out.println("Json : " + json);
			jsonMap = mapper.readValue(json, new TypeReference<HashMap<String, Object>>() {
			});
			System.out.println("Converted Map : " + jsonMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonMap;
	}

	/**
	 * Get Random Index From List
	 * 
	 * @return index
	 */
	public static int getRandomIndex(int size) {
		Random random = new Random();
		int index = random.nextInt(size);
		System.out.println("Returning Index: " + index);
		return index;
	}

	/**
	 * Split String
	 * 
	 * @param text
	 * 
	 * @para pattern
	 * 
	 * @return String[]
	 * 
	 */
	public static String[] splitString(String text, String pattern) {
		splitedString = text.split(pattern);
		// for (int i = 0; i < splitedString.length; i++) {
		// System.out.println(i + " :: " + splitedString[i]);
		// }
		return splitedString;

	}

	/**
	 * MathRound Function
	 * 
	 * @param value
	 * 
	 * @return roundedNumber
	 */
	public static String roundNumber(String value) {
		long roundedNumber = Math.round(Double.valueOf(value));
		System.out.println("Rounded Number: " + roundedNumber);
		return String.valueOf(roundedNumber);
	}

	/**
	 * Method to return all digit from String
	 * 
	 * @param text
	 * 
	 * @return numbers
	 */
	public static String getDigitsFromString(String text) {
		char[] ch = text.toCharArray();
		String number = "";
		for (char c : ch) {
			if (Character.isDigit(c) | Character.toString(c).equals(".")) {
				number += c;
			}
		}
		return number;
	}

	/**
	 * Method to return all letters from String
	 * 
	 * @param text
	 * 
	 * @return numbers
	 */
	public static String getAlphabetsFromString(String text) {
		char[] ch = text.toCharArray();
		String value = "";
		for (char c : ch) {
			if (Character.isLetter(c)) {
				value += c;
			}
		}
		return value;
	}

	/**
	 * Method to return all letters & numbers removing special chars from String
	 * 
	 * @param text
	 * 
	 * @return alphanumeric
	 */
	public static String getAlphaNumericFromString(String text) {
		char[] ch = text.toCharArray();
		String value = "";
		for (char c : ch) {
			if (Character.isLetter(c) | Character.isDigit(c)) {
				value += c;
			}
		}
		return value;
	}

	/**
	 * GET JSON FROM FILE
	 * 
	 * @param jsonFilePath
	 * @return
	 */
	public static DocumentContext getJsonFromFile(String jsonFilePath) {
		FileInputStream json = null;
		try {
			json = new FileInputStream(new File(jsonFilePath + ".json"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		Configuration configuration = Configuration.builder().jsonProvider(new JacksonJsonNodeJsonProvider())
				.mappingProvider(new JacksonMappingProvider()).build();
		return using(configuration).parse(json);
	}

	/**
	 * GET JSON FROM PAYLOAD
	 * 
	 * @param payload
	 * @return
	 */
	public static DocumentContext getJsonFromPayLoad(String payload) {
		Configuration configuration = Configuration.builder().jsonProvider(new JacksonJsonNodeJsonProvider())
				.mappingProvider(new JacksonMappingProvider()).build();
		return using(configuration).parse(payload);
	}

	public static HashMap<String, String> checkFieldAttributesInXMLFromMap(HashMap<String, String> map, String xml) {
		HashMap<String, String> attributeMap = new HashMap<>();
		Iterator<String> iterator = map.keySet().iterator();
		while (iterator.hasNext()) {
			String key = iterator.next();
			String value = map.get(key);
			System.out.println("Key: " + key + "   -   Value:" + value);
			if (value.equalsIgnoreCase("Yes") || value.equalsIgnoreCase("Y")) {
				if (xml.contains(key)) {
					attributeMap.put(key, "Pass, Actual: Value '[" + key + "]' is available");
				} else {
					attributeMap.put(key, "Fail, Actual: Value '[" + key
							+ "]' is not available; Expected: Value should be available");
				}
			} else if (value.equalsIgnoreCase("No") || value.equalsIgnoreCase("N")) {
				if (xml.contains(key)) {
					attributeMap.put(key, "Fail, Actual: Value '[" + key
							+ "]' is available; Expected: Value should not be available");
				} else {
					attributeMap.put(key, "Pass, Actual: Value '[" + key + "]' is not available");
				}
			} else if (value.equalsIgnoreCase("Yes") || value.equalsIgnoreCase("Y") || value.equalsIgnoreCase("No")
					|| value.equalsIgnoreCase("N") || !value.isEmpty()) {
				if (xml.contains(value)) {
					attributeMap.put(key, "Pass, Actual: Value '[" + value + "]' is available for Key '[" + key + "]'");
				} else {
					attributeMap.put(key,
							"Fail, Actual: Value '[" + value + "]' is not available for Key '[" + key + "]'");
				}
			}
		}
		return attributeMap;
	}

	public static JSONArray checkFieldAttributesInXMLFromJson(HashMap<String, String> map, String xml) {
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		Iterator<String> iterator = map.keySet().iterator();
		while (iterator.hasNext()) {
			String key = iterator.next();
			String value = map.get(key);
			String columnName = key;
			String columnValue = "";

			if (key.equalsIgnoreCase("TransactionType")) {
				if (xml.contains(value)) {
					columnValue = "Pass, Actual: Value '[" + value + "]' is available";
				} else {
					columnValue = "Fail, Actual: Value '[" + value + "]' not available";
				}
			} else {
				String xpathValue = evaluateXmlUsingXpath(xml, key);
				System.out.println("Key: " + key + "   -   Value:" + value + " - XPath Return Value: " + xpathValue);

				if (value.equalsIgnoreCase("Yes") || value.equalsIgnoreCase("Y")) {
					if (!xpathValue.isEmpty()) {
						columnValue = "Pass, Key: " + key + " - Value: " + xpathValue;
					} else {
						columnValue = "Fail, Key: " + key + " - Value: " + xpathValue;
					}
				} else if (value.equalsIgnoreCase("No") || value.equalsIgnoreCase("N")
						|| value.equalsIgnoreCase("N/A")) {
					if (xpathValue.isEmpty()) {
						columnValue = "Pass, Key: " + key + " - Value: " + xpathValue + "is not avalibale";
					} else {
						columnValue = "Fail, Key: " + key + " - Value: " + xpathValue + "is avalibale";
					}
				} else if (!value.equalsIgnoreCase("Yes") || !value.equalsIgnoreCase("Y")
						|| !value.equalsIgnoreCase("N/A") || !value.equalsIgnoreCase("No")
						|| !value.equalsIgnoreCase("N") || !value.isEmpty()) {
					if (xpathValue.equalsIgnoreCase(value) || xpathValue.contains(value)) {
						columnValue = "Pass, Expected Value: " + value + " - Actual Value: " + xpathValue;
					} else {
						columnValue = "Fail, Expected Value: " + value + " - Actual Value: " + xpathValue;
					}
				}
			}
			jsonObject.put(columnName, columnValue);
		}
		jsonArray.put(jsonObject);
		return jsonArray;
	}

	public static String evaluateXmlFileUsingXpath(File xmlFile, String xpathExpression) {
		String value = "";
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document xml = db.parse(xmlFile);
			XPath xpath = XPathFactory.newInstance().newXPath();
			value = (String) xpath.evaluate(xpathExpression, xml, XPathConstants.STRING);
			System.out.println("Value" + value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public static String evaluateXmlUsingXpath(String xmlResponse, String xpathExpression) {
		String value = "";
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document xml = db.parse(Data.TARGET_FOLDER_PATH + xmlResponse);
			XPath xpath = XPathFactory.newInstance().newXPath();
			value = (String) xpath.evaluate(xpathExpression, xml).trim();
			System.out.println("XML Value:: " + value.trim());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public static void evaluateJsonPath(String response, String expression) {
		Object value = JsonPath.parse(response).read(expression);
		if (value.toString().isEmpty()) {

		}
	}

	/**
	 * Java main to test utility method
	 */
	public static void main(String args[]) {
//		System.out.println("STRINGS ARE: " + getRandomChars(5));
	}

}
