package eon.qa.testrunner;

import com.intuit.karate.junit5.Karate;

class KarateTestRunner {

	@Karate.Test
	Karate testUsers() {
		return Karate.run("sample").relativeTo(getClass());
	}

	@Karate.Test
	Karate testAll() {
		return Karate.run().relativeTo(getClass());
	}

	@Karate.Test
	Karate testTags() {
		System.setProperty("karate.env", "sample");
		return Karate.run("classpath:").tags("@xmlSample04").relativeTo(getClass());
	}

}
